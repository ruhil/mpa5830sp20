---
authors:
- admin
# bio:  
# education:
#   courses:
#   - course: PhD in Political Science
#     institution: SUNY at Stony Brook (NY) 
#     year: 1999
#   - course: MA in Economics
#     institution: University of New Hampshire (NH)
#     year: 1993
#   - course: BA in Economics
#     institution: University of Mumbai, Mumbai (India)
#     year: 1989
# email: ""
# interests:
# - Data analytics and visualization
# - Public health 
# - Program evaluation
name: Anirudh V. S. Ruhil
organizations:
- name: Ohio University
  url: ""
role: Professor
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/aruhil
- icon: github
  icon_pack: fab
  link: https://github.com/aruhil
superuser: true
user_groups:
- Researchers
- Visitors
---

This is the website for the MPA 5830 -- Data Analytics for Leadership & Public Affairs course offered in our Online Master of PUblic Administration (MPA) program. Use the navigation bar to review the slides (I use them in the video lectures), the vignettes, the practice exercises, and the solutions to these exercises. Answers to the assignments are shared via the #general channel on Slack. 

{{% alert warning %}} Note: Your assignment grades will be posted on this course's Blackboard page. Ditto for the lecture and tutorial videos. {{% /alert %}}
